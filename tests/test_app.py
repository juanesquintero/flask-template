import asyncio
from app import create_app

def test_create_app_dev():
    app = create_app(True) 
    assert app.config['TESTING'] == True
    assert app.config['PRESERVE_CONTEXT_ON_EXCEPTION'] == False


def test_create_app_prod():
    app = create_app() 
    assert app.config['TESTING'] == False
    assert app.config['PRESERVE_CONTEXT_ON_EXCEPTION'] == None

