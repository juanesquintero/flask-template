from requests import get, post, put, patch, delete
from flask import flash

class APIClient():

    __instance = None

    @staticmethod
    def get_instance(api_path=None, headers=None):
        if APIClient.__instance:
            return APIClient.__instance
        else:
            return APIClient(api_path, headers)

    def __init__(self, api_path, headers=None):
        if APIClient.__instance is not None:
            raise Exception('The http API client can only instance once!')
        self.api_path = api_path
        self.headers = headers
        APIClient.__instance = self

    def set_headers(self, headers):
        self.headers = headers

    def request_params(self, endpoint, body=None):
        return dict(
            url=self.api_path + endpoint,
            headers=self.headers,
            json=body
        )

    def check_response(self, res):
        try:
            body = res.json()
        except Exception as e:
            body = res.text
        status = res.status_code
        if (str(status)[0] in ['2', '3']):
            return body
        return flash(f'ERROR in API call: {body}', 'danger')

    def get(self, endpoint):
        res = get(**self.request_params(endpoint))
        return self.check_response(res)

    def post(self, endpoint, body):
        res = post(**self.request_params(endpoint, body))
        return self.check_response(res)

    def put(self, endpoint, body):
        res = put(**self.request_params(endpoint, body))
        return self.check_response(res)

    def patch(self, endpoint, body):
        res = patch(**self.request_params(endpoint, body))
        return self.check_response(res)

    def delete(self, endpoint):
        res = delete(**self.request_params(endpoint))
        return self.check_response(res)
        